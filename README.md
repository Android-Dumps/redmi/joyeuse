## qssi-user 12
12 SKQ1.211019.001 V13.0.5.0.SJZIDXM release-keys
- Manufacturer: xiaomi
- Platform: atoll
- Codename: joyeuse
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Kernel Version: 4.14.190
- Id: SKQ1.211019.001
- Incremental: V13.0.5.0.SJZIDXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/joyeuse_id/joyeuse:12/RKQ1.211019.001/V13.0.5.0.SJZIDXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211019.001-V13.0.5.0.SJZIDXM-release-keys
- Repo: redmi/joyeuse
